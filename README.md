# README #

### What is this repository for? ###

Parrot is the internal name for crowdphrase.com. The original and complete repository is kept private, but I have extracted some key parts of the Django application for demonstration purposes.

### How do I get set up? ###

You don't. This will not run as is.

### Contribution guidelines ###

You don't. This is for demonstration purposes only.
