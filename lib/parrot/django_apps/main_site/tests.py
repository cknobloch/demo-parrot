"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

import datetime
import mock

from django.test import TestCase
from django.contrib.auth.models import User
from django.core.cache import get_cache
from django.utils import timezone

from parrot.django_apps.main_site.models import *
