from django import template
from django import forms
from django.utils.safestring import mark_safe
from django.utils import simplejson
from django.http import QueryDict

register = template.Library()

@register.filter(name='wrap')
def wrap(field, args):
    default_tag = 'div'
    args = QueryDict(args)
    if isinstance(field, forms.Field):
        field = field.as_widget()
    attrs = ' ' + ' '.join('{}={}'.format(k, v)
                           for k, v in args.iteritems()
                           if k != 'tag')
    return mark_safe('<{tag}{attrs}>{field}</{tag}>'.format(field=field,
                                                            tag=args.get('tag', default_tag),
                                                            attrs=attrs))

@register.filter(name='addclass')
def addclass(field, cls):
    return field.as_widget(attrs={'class': cls})

@register.filter(name='jsonify')
def jsonify(object):
    return simplejson.dumps(object)
