from django import template
from django.utils import simplejson

register = template.Library()

@register.filter(name='jsonify')
def jsonify(object):
    return simplejson.dumps(object)
