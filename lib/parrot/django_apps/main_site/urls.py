
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse

from parrot.django_apps.main_site.tokens import PasswordResetTokenGenerator
from parrot.django_apps.main_site.forms  import PasswordResetForm
from parrot.django_apps.main_site.views  import SignInForm

password_reset_token_generator = PasswordResetTokenGenerator()

auth_urlpatterns = patterns('django.contrib.auth.views',                            
    url(r'^logout/$', 'logout', {'template_name': 'main_site/logged_out.html', 'next_page': '/'}, name='logout'),
    #url(r'^logout_then_login/$', 'logout_then_login'),
    #url(r'^passwordchange/$', 'password_change', 
    #    {'template_name': 'main_site/password_change.html',
    #     'post_change_redirect': '/'}),
    url(r'^passwordreset/$', 'password_reset', 
        {'is_admin_site': False,
         'template_name': 'main_site/password_reset.html',
         'email_template_name': 'email/password_reset.html',
         'token_generator': password_reset_token_generator,
         'password_reset_form': PasswordResetForm,
     }, 'passwordreset'),
    url(r'^passwordresetdone/$', 'password_reset_done', 
        {'template_name': 'main_site/password_reset_done.html',
         'extra_context': {'no_reply_address': 'noreply@chatoperated.com'},
     }),
    url(r'^passwordresetconfirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
        'password_reset_confirm', 
        {'template_name': 'main_site/password_reset_confirm.html',
         'post_reset_redirect': '/login/',
         'token_generator': password_reset_token_generator},
        name='password-reset-confirm'))

site_urlpatterns = patterns(
    'parrot.django_apps.main_site.views',
    url(r"^$", "home", name="home"),
    url(r"^robots.txt$", "robots", name="robots"),
    url(r"^chatoperated/$", "chat_operated", name="chat_operated"),        
    url(r"^terms/$",  "terms",  name="terms"),
    url(r"^signup/$", "signup", name="signup"),    
    url(r"^signupconfirm/$", "signup_confirm", name="signup-confirm"),    
    url(r'^login/$',  'login',  {'template_name': 'main_site/login.html', 'authentication_form': SignInForm}, name='login'),
    url(r"^member/$", "member_home", name="member-home"),
    url(r"^member/verify/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$", "member_verify", name="member-verify"),
    url(r"^api/v1/user/$", "user_view", name="user"),
    url(r"^api/v1/user/(?:(?P<user_id>\d+)/)$", "user_view", name="user"),
    url(r"^api/v1/user-stats/$", "user_stats", name="user-stats"),    
    url(r"^api/v1/user-notification/(?:(?P<user_notification_id>\d+)/)?$", "user_notification", name="user-notification"),
    url(r"^api/v1/phrase-request/$", "phrase_request", name="phrase-request"),
    url(r"^api/v1/phrase-response/$", "phrase_response", name="phrase-response"),
    url(r"^api/v1/phrase-request/(?P<phrase_request_id>\d+)/$",
        "phrase_request", name="phrase-request"),
    url(r"^api/v1/phrase-response/(?P<phrase_response_id>\d+)/$",
        "phrase_response", name="phrase-response"),
    url(r"^api/v1/phrase-response/(?P<phrase_response_id>\d+)/clip/$", "phrase_response_clip", name="phrase-response-clip"),    
    url(r"^api/v1/invite/$", 'invite', name="invite"),    
    )

urlpatterns = patterns('',
    url(r'^', include(site_urlpatterns)),
    url(r'^', include(auth_urlpatterns))
)

if settings.DEBUG:
    urlpatterns += patterns(
        'parrot.django_apps.main_site.views',
        url(r"^test/$", "test", name="test"),
    )

handler500 = 'parrot.django_apps.main_site.views.handler500'
