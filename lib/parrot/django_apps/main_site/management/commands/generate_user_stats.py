
from parrot.django_apps.main_site.models import UserProfile

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    def handle(self, *args, **options):
        user_profiles = sorted(UserProfile.objects.filter(user__is_active=True).order_by('id'), 
                               key=lambda x: -x.get_ranking_score())
        for ranking, user_profile in enumerate(user_profiles):
            user_profile.ranking = ranking + 1
            user_profile.save()
