
from parrot.django_apps.main_site.models import UserNotification

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

class Command(BaseCommand):
    def handle(self, *args, **options):
        for user_notification in UserNotification.objects.filter(
                ack_date=None, expiration__lte=timezone.now()):
            user_notification.expire()
