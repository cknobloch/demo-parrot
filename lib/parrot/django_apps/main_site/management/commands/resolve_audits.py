
import datetime

from convoshare.django_apps.main_site.models import AUDIT_LIFETIME  # , AUDIT_STATUSES, PhraseResponse 

from django.core.management.base import BaseCommand
from django.utils import timezone

def resolve_audits(lifetime=AUDIT_LIFETIME):
    now = timezone.now()
    audit_expiration_delta = datetime.timedelta(days=lifetime)
    # for phrase_response in PhraseResponse.objects.filter(status__in=AUDIT_STATUSES):
    #     if (phrase_response.closed+audit_expiration_delta) < now:
    #         phrase_response.resolve_audit(lock=True)

class Command(BaseCommand):
    def handle(self, *args, **options):
        resolve_audits()
