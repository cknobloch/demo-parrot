
from parrot.utils import SingleInstanceAssertion
from parrot.django_apps.main_site.models import Mail

from django.core.management.base import BaseCommand, CommandError        

class Command(BaseCommand):
    def handle(self, *args, **options):
        with SingleInstanceAssertion(__file__):
            for mail in Mail.objects.filter(sent=None).order_by('id'):
                mail.send()
