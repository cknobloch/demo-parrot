
from django.core.urlresolvers import reverse
from django.template import Context, Template
from django.template import loader
from django.utils.translation import ugettext, ugettext_lazy as _, get_language
from django.utils.http import int_to_base36
from django.utils.safestring import mark_safe
from django.contrib.auth.models import User
from django.contrib.sites.models import Site, get_current_site
from django.conf import settings

from parrot import SITE_FULL_ADDRESS
from parrot.django_apps.main_site.tokens import default_token_generator
from parrot.django_apps.main_site.models import Mail

def get_site_vars(site):
    if settings.PUBLIC:
        domain = settings.DOMAIN or site.domain
    else:
        domain = site.domain
    protocol = settings.PROTOCOL
    host = '{protocol}://{domain}'.format(protocol=protocol, domain=domain)
    return dict(protocol=protocol,
                domain=domain,
                host=host,
                site_name=site.name,
                site_full_address=SITE_FULL_ADDRESS,
                STATIC_URL=settings.STATIC_URL)

class MailFactory(object):
    subject = None
    body = None
    template_name = None
    template = None
    
    def __init__(self, **data):
        data.update(**get_site_vars(Site.objects.get_current()))
        self.data = data

    def get_subject(self, data):
        return self.subject and self.subject.format(**data)
    
    def get_body(self, data):
        if self.body:
            return self.body.format(**data)
        if self.template:
            return Template(self.template).render(Context(data))
        if self.template_name:
            return loader.render_to_string(self.template_name, data)

    def get_data(self, to_email):
        data = self.data.copy()
        try:
            user = User.objects.get(username=to_email)            
        except User.DoesNotExist:
            pass
        else:
            data.update(self.get_user_data(user))
        return data

    def get_user_data(self, user):
        return {'to_name': user.get_profile().nickname}

    def create_mail(self, to_email, save=True):
        data = self.get_data(to_email)
        mail = Mail()
        mail.to_email = to_email
        mail.subject = self.get_subject(data)
        mail.body = self.get_body(data)
        if save:
            mail.save()
        return mail

class NonMemberEmail(MailFactory):
    template_name = 'email/base.html'

class MemberEmail(MailFactory):
    template_name = 'email/member_base.html'

class SignUpEmail(NonMemberEmail):    
    subject  = _('Welcome to {site_name}')
    template = """{% extends "email/base.html" %}{% load url from future %}{% block body %}<p>Thanks for joining our phrase sharing community.<br/><a href="{{ host }}{% url 'member-verify' uidb36=uidb36 token=token %}">Sign in</a> to verify your email address and start listening to phrases now!</p>{% endblock %}"""

    def get_user_data(self, user):
        data = super(SignUpEmail, self).get_user_data(user)
        data.update({
            'uidb36': int_to_base36(user.id),
            'token' : default_token_generator.make_token(user, login_timestamp='')
        })
        return data

class FreeCreditEmail(MemberEmail):    
    subject  = _('You received a free credit')    
    template = """{% extends "email/member_base.html" %}{% block body %}<p>As an active member of the phrase speaking community, you've been awarded a credit!</p>{% endblock %}"""

class PasswordResetEmail(MemberEmail):
    subject  = _('Forgot your {site_name} password?')
    template = """{% extends "email/member_base.html" %}{% load url from future %}{% block body %}<p>Follow the link below to reset your password.</p><p>{{ host }}{% url 'django.contrib.auth.views.password_reset_confirm' uidb36=uidb36 token=token %}</p>{% endblock %}"""
    
    def __init__(self, token_generator):
        super(PasswordResetEmail, self).__init__()
        self.token_generator = token_generator

    def get_user_data(self, user):        
        data = super(PasswordResetEmail, self).get_user_data(user)
        data.update({
            'uidb36': int_to_base36(user.id),
            'token' : self.token_generator.make_token(user)
        })
        return data

class InviteEmail(NonMemberEmail):
    subject  = _('A friend has invited you to join')
    template = """{% extends "email/member_base.html" %}{% block body %}<p>Are you learning a foreign language or dialect? {{ user.email }} thought you'd be interested in joining our phrase speaking community. Learn more and get started at <a href="{{ host }}">{{ domain }}</a>.</p>{% endblock %}"""
