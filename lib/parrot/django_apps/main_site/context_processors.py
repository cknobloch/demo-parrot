
from django.utils.safestring import mark_safe
from django.utils import simplejson, timezone
from django.contrib.sites.models import get_current_site
from django.conf import settings

from parrot.django_apps.main_site.views import get_active_sessions, get_active_user_count_per_language
from parrot.utils.django_helpers import to_js_repr, format_metric

def to_js(data):
    return mark_safe(simplejson.dumps(to_js_repr(data)))    

def base_context(request):
    site = get_current_site(request)
    protocol = getattr(settings, 'PROTOCOL', 'http')
    domain = settings.DOMAIN or site.domain
    host = '{protocol}://{domain}'.format(protocol=protocol, domain=domain)
    auth_token = getattr(request.user, 'auth_token', None)
    return {
        'debug'      : settings.DEBUG,
        # 'total_online_users' : len(get_active_sessions()),        
        'total_users_per_language': get_active_user_count_per_language(),
        'site_name'  : site.name,
        'site'       : site,
        'protocol'   : protocol,
        'domain'     : domain,
        'host'       : host,
        'mail_domain': domain[4:] if domain.startswith('www.') else domain,
        'no_reply_address': settings.NO_REPLY_EMAIL,
        'auth_token' : auth_token and auth_token.key
    }
