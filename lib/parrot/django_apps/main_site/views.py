
import datetime
import copy
from functools import wraps
#import logging; logger = logging.getLogger(__name__)

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, HttpResponseNotFound, HttpResponseForbidden
from django.http import QueryDict
from django.template import RequestContext, loader
from django.utils.decorators import available_attrs
from django.utils.translation import ugettext, ugettext_lazy as _, get_language
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.views.generic import View
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache, cache_page
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import classonlymethod, method_decorator
from django.utils import simplejson, timezone
from django.utils.http import base36_to_int
from django.utils.safestring import mark_safe
from django import forms
from django.db.models import Count, Max, Q
from django.db import transaction, IntegrityError
from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, AnonymousUser
from django.contrib.sessions.models import Session
from django.contrib.sites.models import get_current_site

# For login(), taken from django.contrib.auth
import urlparse
from django.conf import settings
from django.template.response import TemplateResponse
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, logout as auth_logout
from django.contrib.auth.forms import AuthenticationForm

from parrot.utils.language import *
from parrot.utils.django_helpers import JsonResponse, authenticated_or_401, model_to_dict, js_resource_to_response, format_datetime, format_datetime_html, from_simple_repr, to_js_repr, memoize
from parrot.django_apps.main_site.models import UserProfile, Country, TermsOfService, UserNotification, AuthToken, AUDIT_LIFETIME
from parrot.django_apps.main_site.forms  import *
from parrot.django_apps.main_site.emails import *
from parrot.django_apps.main_site.tokens import default_token_generator

CACHE_TIMEOUT = 60 * 5
MAX_LIMIT = 30

def unsigned_int(v):
    """Returns the signed integer as its long-type unsigned equivalent."""
    return long(v) & 0xFFFFFFFF

def handler500(request):
    if request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest':
        import sys, traceback
        (exc_type, exc_info, tb) = sys.exc_info()
        message = "%s\n" % exc_type.__name__
        message += "%s\n\n" % exc_info
        message += "TRACEBACK:\n"    
        for tb in traceback.format_tb(tb):
            message += "%s\n" % tb
        status = 500
        return JsonResponse({'status': status, 'message': message}, status=status)
    else:
        response = render_to_response("500.html", {},
                                      context_instance=RequestContext(request))
        response.status_code = 500
        return response
    
@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.REQUEST.get(redirect_field_name, '')
    current_site = get_current_site(request)

    if request.method == "POST":
        form = authentication_form(data=request.POST)
        if form.is_valid():
            netloc = urlparse.urlparse(redirect_to)[1]

            # Use default setting if redirect_to is empty
            if not redirect_to:
                redirect_to = settings.LOGIN_REDIRECT_URL

            # Heavier security check -- don't allow redirection to a different
            # host.
            elif netloc and netloc != request.get_host():
                redirect_to = settings.LOGIN_REDIRECT_URL
            
            user = form.get_user()            
            if not user.get_profile().email_verified:
                SignUpEmail().create_mail(user.email)
                return HttpResponseRedirect(reverse('signup-confirm'))

            # Okay, security checks complete. Log the user in.
            auth_login(request, user)
            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    request.session.set_test_cookie()

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)

def robots(request):
    return HttpResponse("User-Agent: *\nDisallow: \n", content_type='text/plain')

def chat_operated(request):
    return render(request, 'main_site/chat_operated.html')

def terms(request):
    terms = TermsOfService.objects.order_by('-effective_date')[0]
    terms_params = {'audit_lifetime': AUDIT_LIFETIME}
    context = {
        'site': get_current_site(request),
        'terms': mark_safe(terms.terms.format(**terms_params)),
        'effective_date': format_datetime(terms.effective_date)
    }
    return render(request, 'main_site/terms.html', context)

def signup_confirm(request):    
    context = {
    }
    return render(request, 'main_site/signup_confirm.html', context)

@login_required
def signup(request):
    # request.session.set_test_cookie()

    if request.user.get_profile().terms_accepted:
        return HttpResponseRedirect(reverse('member-home'))

    current_site = get_current_site(request)
    context = {
        'signup_form': UserProfileForm(request.user),
        'site': current_site,
        'site_name': current_site.name,
    }

    return render(request, 'main_site/signup.html', context)

def home(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('member-home'))
    else:        
        context = {
            'signup_form': SignUpForm(),
            'coming_soon': False,  # not settings.DEBUG
        }
        return render(request, 'main_site/home.html', context)

# Doesn't need csrf_protect since no-one can guess the URL
@sensitive_post_parameters()
@never_cache
def member_verify(request, uidb36=None, token=None):
    """
    View that checks the hash in a password reset link and presents a
    form for entering a new password.
    """
    assert uidb36 is not None and token is not None  # checked by URLconf
    try:
        uid_int = base36_to_int(uidb36)
        user = User.objects.get(id=uid_int)
    except (ValueError, User.DoesNotExist):
        user = None

    if user is not None and \
       default_token_generator.check_token(user, token, login_timestamp=''):
        # Mark as "email verified"
        user_profile = user.get_profile()
        user_profile.email_verified = True
        user_profile.save()

    return HttpResponseRedirect(reverse('member-home'))

@login_required
def member_home(request):
    messages = []
    user_profile = request.user.get_profile()
    if not user_profile.terms_accepted:
        return HttpResponseRedirect(reverse('signup'))
    latest_terms_effective_date = \
        TermsOfService.objects.all().aggregate(Max('effective_date'))['effective_date__max']
    if user_profile.terms_accepted < latest_terms_effective_date:
        terms_changed_message = _('The <a href="{terms_url}">Terms of Service</a> have been updated and will take effect on {effective_date}. <a id="agree-to-terms" href="javascript:void(0);">I agree to the updated terms</a>').format(
            terms_url=reverse('terms'),
            effective_date=format_datetime_html(latest_terms_effective_date))
        messages.append(mark_safe(terms_changed_message))

    user_profile_form = UserProfileForm(request.user)
    user_profile_form.fields['terms_accepted'].widget = forms.HiddenInput()

    context = {
        'site_messages'     : messages,
        'user_profile'      : user_to_dict(request.user, is_self=True),
        'user_profile_form' : user_profile_form,
        'user_notifications': 
            [mark_safe(simplejson.dumps(to_js_repr(user_notification_to_dict(user_notification)))) for user_notification in UserNotification.objects.filter(user=request.user, ack_date=None)],
        'default_nickname'  : _('Anonymous'),
        'language_choices'  : LANGUAGE_CODE_CHOICES,
    }
    
    return render(request, 'main_site/member_home.html', context)

# @memoize(timeout=60)
def get_active_sessions():
    active_sessions = []
    for session in Session.objects.filter(expire_date__gte=timezone.now()):
        data = session.get_decoded()
        last_request_time = data.get('last_request_time')
        if last_request_time and \
           last_request_time >= (datetime.datetime.now()-datetime.timedelta(minutes=10)):
            active_sessions.append(data)
    return active_sessions

@memoize(timeout=60)
def get_active_user_count_per_language():
    user_counts = {language_code: 0 for language_code in TOP_LANGUAGES}
    user_ids = set(session.get('_auth_user_id') for session in get_active_sessions())
    # compute actual counts
    user_profiles = UserProfile.objects.filter(user_id__in=user_ids)
    for user_profile in user_profiles:
        user_counts[user_profile.language_code] += 1
    user_counts = sorted([(LANGUAGES_NATIVE_NAMES[k], v)
                          for k, v in user_counts.iteritems()],
                         key=lambda (k, v): (-v, k))
    user_counts.insert(0, (_('Language'), len(user_profiles)))
    return user_counts

def user_to_dict(user, is_self=False, editable=False):
    profile = user.get_profile()
    rating  = None  # profile.get_satisfaction_rating()
    user_dict = {
        'nickname'     : profile.nickname,        
        'age'          : profile.age,
        'gender'       : profile.gender,        
        'language'     : profile.language,
        'language_code': profile.language_code,
        'country_code' : profile.country.code,
        'country_name' : profile.country.name,                
        'rating'       : rating if rating is None else int(round(rating * 10)),
        'signature'    : profile.signature(),
        }
    if is_self:
        phrase_requests  = user.phrase_requests .filter(hidden=False)
        phrase_responses = user.phrase_responses.filter(hidden=False)
        user_dict.update({
            'is_admin'  : profile.is_admin,                     
            'birth_date': profile.birth_date,
            'phrase_request_count': len(phrase_requests),
            'interest_language': profile.interest_language,
            'interest_language_code': profile.interest_language_code,            
        })
        
    return user_dict

def user_notification_to_dict(user_notification):
    return {
        'id'                : user_notification.id,
        'user_id'           : user_notification.user_id,
        'notification_id'   : user_notification.notification.id,
        'notification_name' : user_notification.notification.name,
        'ack_date'          : user_notification.ack_date,
        'created'           : user_notification.created,
        'created_str'       : format_datetime_html(user_notification.created)
    }

def phrase_request_to_dict(phrase_request, user=None):
    response_ids = PhraseResponse.objects.filter(phrase_request=phrase_request,
                                                 hidden=False).values_list('id', flat=True)

    phrase_request_dict = {
        'id'                : phrase_request.id,
        'text'              : phrase_request.phrase.text,
        'language_code'     : phrase_request.phrase.language_code,        
        'note'              : None,  # phrase_request.note,
        'responder_age_min' : phrase_request.responder_age_min,
        'responder_age_max' : phrase_request.responder_age_max,
        'responder_gender'  : phrase_request.responder_gender,
        'responder_country' : phrase_request.responder_country,
        'created'           : phrase_request.created,                
        'is_mine'           : user and (phrase_request.user_id == user.id),
        'is_native_language': user and (phrase_request.phrase.language_code ==
                                        user.get_profile().language_code),
        'top_response_id'   : response_ids and response_ids[0],
        'response_count'    : len(response_ids),
    }

    my_recent_responses = PhraseResponse.objects.filter(        
        phrase_request=phrase_request,
        user=user,
        hidden=False).order_by('-id')
    if my_recent_responses:
        phrase_request_dict['my_recent_response'] = \
            phrase_response_to_dict(my_recent_responses[0], user=user)
        
    return phrase_request_dict

def phrase_response_to_dict(phrase_response, user=None):
    phrase_response_dict = {
        'id'  : phrase_response.id,
        'text': phrase_response.phrase.text,
        'language_code': phrase_response.phrase.language_code,
        'favorited': user.phrase_response_favorites.filter(
            phrase_response=phrase_response).exists(),
        'created': phrase_response.created,        
        'is_mine': user and (phrase_response.user_id == user.id),
        'hidden' : phrase_response.hidden
    }

    if phrase_response.user:
        user_dict = user_to_dict(phrase_response.user)
        phrase_response_dict.update({'user_{}'.format(k): v for k, v in user_dict.iteritems()})

    return phrase_response_dict

class ResourceView(View):
    authenticated_methods = None

    def authenticate_token(self, request):
        try:
            request.user = AuthToken.objects.select_related('user').get(
                key=request.META.get('HTTP_X_AUTHTOKEN')).user
        except AuthToken.DoesNotExist:
            # FIXME? -- is a correct token really necessary
            # if a user is logged in? 
            # or should we trust a logged in user?
            request.user = AnonymousUser()

    def dispatch(self, request, *args, **kwargs):
        request_method = request.method.lower()
        if (self.authenticated_methods is None or
            request_method in self.authenticated_methods):
            self.authenticate_token(request)
            if not request.user.is_authenticated():
                return HttpResponse(status=401)
        try:
            return super(ResourceView, self).dispatch(request, *args, **kwargs)
        except ObjectDoesNotExist:
            return HttpResponseNotFound()

    def apply_list_constraints(self, queryset, options):
        """
        Returns the given list of models with constraints applied.
        A negative limit indicates that we should reverse the order of the list.
        """
        if not queryset:
            return queryset
        key   = options.get('key')
        limit = int(options.get('limit', MAX_LIMIT))
        desc  = limit < 0
        if desc:
            if key is not None:
                queryset = queryset.filter(id__lt=unsigned_int(key)).order_by('-id')
            else:
                queryset = queryset.all().order_by('-id')
        else:
            if key is not None:
                queryset = queryset.filter(id__gt=unsigned_int(key))
            else:
                queryset = queryset.all()
        return queryset[:min(abs(limit), MAX_LIMIT)]

    def as_js(self, model_or_list):
        return js_resource_to_response(model_or_list)        
        
class UserView(ResourceView):
    http_method_names = ['get', 'post', 'put']
    authenticated_methods = ['get']

    def get(self, request, user_id=None):
        """Get a user profile."""
        if user_id is None:
            user = request.user
        else:
            user = User.objects.get(id=user_id)
        return js_resource_to_response(
            user_to_dict(user, is_self=(user==request.user), editable=True))

    def post(self, request):
        """Create a bare user."""
        form = SignUpForm(data=request.POST)
        if not form.is_valid():
            return HttpResponseBadRequest(js_resource_to_response(form.errors))
        form.save(request=request)
        return HttpResponse(status=200)

    def put(self, request):
        """Update a user (change password, update profile, verify email)."""
        def handle_request(request):
            if request.user.get_profile().terms_accepted:
                form_cls = UserProfileForm
            else:
                form_cls = UserProfileCreationForm
            form = form_cls(request.user, QueryDict(request.raw_post_data))
            if not form.is_valid():
                return HttpResponseBadRequest(js_resource_to_response(form.errors))
            form.save()
            return HttpResponse(status=200)
        return authenticated_or_401(handle_request)(request)

class UserStatsView(ResourceView):
    http_method_names = ['get']

    def get(self, request):
        user = User.objects.get(id=request.GET.get('user_id'))
        return js_resource_to_response(user.get_profile().get_user_stats())

class UserNotificationView(ResourceView):
    http_method_names = ['get', 'delete']

    def get(self, request, user_notification_id=None):
        user_notification = UserNotification.objects.get(id=user_notification_id,
                                                         user=request.user,
                                                         ack_date=None)
        return js_resource_to_response(user_notification_to_dict(user_notification))

    def delete(self, request, user_notification_id=None):
        user_notification = UserNotification.objects.get(id=user_notification_id,
                                                         user=request.user,
                                                         ack_date=None)
        user_notification.ack_date = timezone.now()
        user_notification.save()
        return js_resource_to_response(user_notification_to_dict(user_notification))
    
def tags_to_phrase_request_filters(tags):
    filters = {}
    for k, v in tags.iteritems():
        if v is None:
            continue
        if   k == 'country':
            filters['responder_country__code'] = v
        elif k == 'age':
            filters['responder_age_min__lte'] = v
            filters['responder_age_max__gte'] = v
        elif k == 'gender':
            filters['responder_gender'] = v[0]
    return filters

def tags_to_phrase_request_fields(tags):
    fields = {}
    for k, v in tags.iteritems():
        if v is None:
            continue
        if   k == 'country':
            fields['responder_country'] = Country.objects.get(code=v)
        elif k == 'age':
            v = v / 10 * 10
            fields['responder_age_min'] = v
            fields['responder_age_max'] = v + 9
        elif k == 'gender':
            fields['responder_gender'] = v[0]
    return fields

class PhraseRequestView(ResourceView):
    http_method_names = ['get', 'post', 'delete']
    authenticated_methods = ['post', 'delete']

    def get(self, request):
        """Get phrase requests."""
        audited_ids = PhraseRequest.audits_for_class(user=request.user)\
                                   .values_list('entity_id', flat=True)
        phrase_requests = PhraseRequest.objects.filter(hidden=False).exclude(id__in=audited_ids)

        params = {}
        
        if request.GET.get('language_code'):
            params['phrase__language_code'] = request.GET['language_code']
            
        if request.GET.get('text'):
            text, tags = extract_hashtags(request.GET['text'])            
            params['phrase__text__icontains'] = text
            params.update(tags_to_phrase_request_filters(tags))
            
        if params:
            phrase_requests = phrase_requests.filter(**params)

        # Apply broad "source" filters
        if request.GET.get('source'):
            source = request.GET['source']
            if   source == 'my-requests':
                phrase_requests = phrase_requests.filter(user=request.user)
            elif source == 'favorites':
                fav_ids = request.user.phrase_response_favorites\
                                      .all()\
                                      .values_list('phrase_response_id', flat=True)
                phrase_requests = phrase_requests.filter(phrase_responses__id__in=fav_ids)
            elif source == 'my-qualified':
                reqs = []
                user_profile = request.user.get_profile()
                if user_profile.gender:
                    phrase_requests = phrase_requests.filter(
                        Q(responder_gender__isnull=True)|
                        Q(responder_gender=user_profile.gender))                
                if user_profile.country:
                    phrase_requests = phrase_requests.filter(
                        Q(responder_country__isnull=True)|
                        Q(responder_country=user_profile.country))              
                if user_profile.age:
                    phrase_requests = phrase_requests.filter(
                        Q(responder_age_min__isnull=True)|Q(responder_age_max__isnull=True)|
                        Q(responder_age_min__lte=user_profile.age,
                          responder_age_max__gte=user_profile.age))

        phrase_requests = self.apply_list_constraints(phrase_requests, request.GET)        

        return self.as_js([phrase_request_to_dict(phrase_request, request.user)
                           for phrase_request in phrase_requests])

    def post(self, request):
        """Create a phrase request."""
        form = PhraseRequestForm(data=request.POST)
        if not form.is_valid():
            return HttpResponseBadRequest(js_resource_to_response(form.errors))

        phrase, created = Phrase.objects.get_or_create(
            text=form.cleaned_data['text'],
            language_code=form.cleaned_data['language_code'])
        
        fields = tags_to_phrase_request_fields(form.cleaned_data['tags'])
        
        phrase_request, created = PhraseRequest.objects.get_or_create(
            user=request.user,
            hidden=False,
            phrase=phrase,
            **fields)
        
        return self.as_js([phrase_request_to_dict(phrase_request, request.user)])
    
    def delete(self, request, phrase_request_id):
        phrase_request = PhraseRequest.objects.get(id=int(phrase_request_id))

        if phrase_request.user == request.user:
            phrase_request.hidden = True
        else:
            phrase_request.mark_offensive(request.user)
            
        phrase_request.save()

        return self.as_js(phrase_request_to_dict(phrase_request, request.user))

class PhraseResponseView(ResourceView):
    http_method_names = ['get', 'post', 'put', 'delete']
    authenticated_methods = ['post', 'put', 'delete']
    
    def get(self, request):
        """Get phrase responses."""
        phrase_request_id = request.GET.get('phrase_request_id')
        audited_ids = PhraseResponse.audits_for_class(user=request.user)\
                                    .values_list('entity_id', flat=True)
        phrase_responses = PhraseResponse.objects.exclude(id__in=audited_ids).filter(hidden=False)
        if phrase_request_id:
            phrase_responses = phrase_responses.filter(phrase_request_id=phrase_request_id)
        phrase_responses = self.apply_list_constraints(
            phrase_responses,
            request.GET)
        return self.as_js([phrase_response_to_dict(phrase_response, user=request.user)
                           for phrase_response in phrase_responses])

    def post(self, request):
        """Create a phrase response."""
        form = PhraseResponseForm(data=request.POST)
        if not form.is_valid():
            return HttpResponseBadRequest(js_resource_to_response(form.errors))

        params = form.cleaned_data
        
        if params['text']:            
            phrase, created = Phrase.objects.get_or_create(
                text=params['text'],
                language_code=request.user.get_profile().language_code)
        else:
            phrase_request  = PhraseRequest.objects.get(id=params['phrase_request_id'])
            phrase, created = phrase_request.phrase, False
            
        phrase_response = PhraseResponse.objects.create(
            user=request.user,
            phrase_request_id=params['phrase_request_id'],
            phrase=phrase)
        
        return self.as_js([phrase_response_to_dict(phrase_response, user=request.user)])        

    def put(self, request, phrase_response_id):
        """
        Update the phrase response. 
        At the moment, this can only be used to mark user favorites.
        """
        form = PhraseResponseForm(data=QueryDict(request.raw_post_data))
        if not form.is_valid():
            return HttpResponseBadRequest(js_resource_to_response(form.errors))

        params = form.cleaned_data
        phrase_response = PhraseResponse.objects.get(id=phrase_response_id)

        if params.get('favorited') is not None:
            fav, created = PhraseResponseFavorite.objects.get_or_create(
                phrase_response_id=phrase_response_id,
                user=request.user)
            if not created:
                fav.delete()
        else:            
            if phrase_response.user != request.user:
                return HttpResponseForbidden()
            
            if params.has_key('text'):
                phrase, created = Phrase.objects.get_or_create(
                    text=params['text'],
                    language_code=request.user.get_profile().language_code)
                phrase_response.phrase = phrase
                
            phrase_response.save()
            
        return self.as_js(phrase_response_to_dict(phrase_response, user=request.user))

    def delete(self, request, phrase_response_id):
        phrase_response = PhraseResponse.objects.get(id=int(phrase_response_id))
        
        if phrase_response.user == request.user:
            phrase_response.hidden = True
        else:
            phrase_response.mark_offensive(request.user)
            
        phrase_response.save()

        return self.as_js(phrase_response_to_dict(phrase_response, user=request.user))

class PhraseResponseClipView(ResourceView):
    http_method_names = ['get', 'post', 'put']
    authenticated_methods = ['post', 'put']
    
    def get(self, request, phrase_response_id):
        """Get clip for phrase response."""        
        phrase_response = PhraseResponse.objects.get(id=int(phrase_response_id))
        #response = HttpResponse()
        #response['Content-Type'] = 'audio/x-wav'
        #return response
        return js_resource_to_response({'url': phrase_response.clip_url})

    def put(self, request, phrase_response_id):
        """Create a clip for a phrase response."""
        phrase_response = PhraseResponse.objects.get(id=int(phrase_response_id))
        if phrase_response.user != request.user:
            return HttpResponseForbidden()
        length = int(request.META['CONTENT_LENGTH'])
        if not request.META['CONTENT_TYPE'].startswith('audio/x-wav'):
            return HttpResponseBadRequest(
                'Unsupported format {}'.format(request.META['CONTENT_TYPE']))
        return HttpResponseRedirect(phrase_response.clip_upload_url)

    def post(self, request, phrase_response_id):  # this should be put, but Wami uses POST
        """Create a clip for a phrase response."""
        phrase_response = PhraseResponse.objects.get(id=int(phrase_response_id))
        if phrase_response.user != request.user:
            return HttpResponseForbidden()
        length = int(request.META['CONTENT_LENGTH'])
        if not request.META['CONTENT_TYPE'].startswith('audio/x-wav'):
            return HttpResponseBadRequest(
                'Unsupported format {}'.format(request.META['CONTENT_TYPE']))
        phrase_response.save_clip_to_storage(request.read(length))
        phrase_response.hidden = False
        phrase_response.save()
        return HttpResponse(status=200)
    

class InviteView(ResourceView):
    http_method_names = ['post']
    
    def post(self, request):        
        try:
            form = InviteForm(request.POST)            
            if not form.is_valid():
                return HttpResponseBadRequest()
            form.save(request.user)
        except ValueError, ve:
            if str(ve) == 'exists':
                return HttpResponse(status=409)
            return HttpResponseBadRequest()
        else:
            return HttpResponse(status=200)

user_view  = UserView.as_view()
user_stats = UserStatsView.as_view()
user_notification = UserNotificationView.as_view()
phrase_request = PhraseRequestView.as_view()
phrase_response = PhraseResponseView.as_view()
phrase_response_clip = csrf_exempt(PhraseResponseClipView.as_view())
invite = InviteView.as_view()

if settings.DEBUG:
    import pytz

    @login_required
    def test(request):
        context = {
        }    
        return render(request, 'main_site/test.html', context)

    # def datetime_from_isoformat(dt):
    #     return datetime.datetime.strptime(dt, "%Y-%m-%dT%H:%M:%S.%fZ")

    # class TestChatTokenView(View):
    #     http_method_names = ['get']

    #     def get(self, request):
    #         tzinfo = pytz.utc # pytz.timezone('US/Eastern')
    #         return js_resource_to_response(
    #             {'token': channel_token_generator.make_session_token(
    #                 request.GET['host'],
    #                 int(request.GET['channel']),
    #                 int(request.GET['subscriber_id']),
    #                 int(request.GET['timeout']),
    #                 now=datetime_from_isoformat(request.GET['live_at']).replace(tzinfo=tzinfo))})

    # test_chat_token = never_cache(authenticated_or_401(TestChatTokenView.as_view()))
