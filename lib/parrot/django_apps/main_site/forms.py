# -*- coding: utf-8 -*-

import re
import datetime

from django import forms
from django.forms.util import flatatt
from django.template import loader
from django.utils.encoding import smart_str
from django.utils.http import int_to_base36
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext, ugettext_lazy as _, get_language
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.hashers import UNUSABLE_PASSWORD, is_password_usable, get_hasher
from django.contrib.sites.models import get_current_site
from django.conf import settings

from parrot.django_apps.main_site.models import *
from parrot.django_apps.main_site.emails import SignUpEmail, PasswordResetEmail, get_site_vars
from parrot.django_apps.main_site.tokens import default_token_generator
from parrot.utils.language import *
from parrot import SITE_FULL_ADDRESS

MIN_AGE = 13

SEPARATOR_CHOICE = None  # FIXME/DEFINE ME as: "<option disabled>──────────</option>"
LANGUAGE_CODE_CHOICES = sorted(LANGUAGES_NATIVE_NAMES.items(), key=lambda (a, b): b)

def add_years(dt, years):
    return dt.replace(year=dt.year+years)

def subtract_years(dt, years):
    return add_years(dt, -years)

def form_labels_to_placeholders(form, exclude=()):
    for k, f in form.fields.iteritems():
        if k in exclude:
            continue
        if isinstance(f.widget, (forms.TextInput, forms.PasswordInput)):
            f.widget.attrs['placeholder'] = f.label

class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'duplicate_username': _("A user with that username already exists. Did you mean to sign in?"),  # <a href='{login_url}'>sign in</a>
    }
    username = forms.EmailField(label=_("Email"), max_length=75)
    password = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ("username",)

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"].lower()
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        error_message = self.error_messages['duplicate_username'].format(login_url=reverse('login'))
        raise forms.ValidationError(error_message)

    def save(self, request=None, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = user.username
        user.set_password(self.cleaned_data["password"])
        
        if commit:
            user.save()
        
        SignUpEmail().create_mail(user.email)

        return user

class UserProfileForm(forms.Form):
    """
    A form to update a user profile.
    """
    nickname = forms.CharField(label=_('Nickname'), max_length=30, required=False)
    age = forms.IntegerField(label=_('Age'), widget=forms.Select, min_value=0, max_value=100,
                             required=False)
    gender = forms.ChoiceField(label=_('Gender'),
                               choices=(('m', 'Male'), ('f', 'Female')),
                               required=False)
    country = forms.ChoiceField(label=_('Country'), required=False)
    language = forms.ChoiceField(label=_('Language'), choices=LANGUAGE_CODE_CHOICES, required=False)
    interest_language = forms.ChoiceField(label=_('Language'), choices=LANGUAGE_CODE_CHOICES,
                                          required=False)    
    terms_accepted = forms.BooleanField(label=_('Terms accepted'), required=False)

    def __init__(self, user, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)

        country_field = self.fields['country']
        country_field.choices = [('', 'Country')]
        country_field.choices.extend(sorted([(country['code'], country['name']) for country in Country.objects.all().values('code', 'name')], key=lambda x: x[1]))

        age_field = self.fields['age']
        age_choices = [(x, x) for x in xrange(MIN_AGE, age_field.max_value)]
        age_choices.insert(0, (0, _('Under {min_age_in_years}').format(min_age_in_years=MIN_AGE)))
        age_field.widget.choices = age_choices
        
        self.fields['terms_accepted'].label = \
            mark_safe(_('I have read and agree to the <a href="{terms_url}">Terms of Service</a>.').format(terms_url=reverse('terms')))

        self._user = user
        
        user_profile = user.get_profile()
        if not user_profile.terms_accepted:
            # Set initial value for "native language" our best guess
            # LCID => Language code
            language_code = user_profile.language_code
            if language_code is None:
                language_code, country_code = \
                    [x.lower() for x in get_language().split('-')]
            self.fields['language'].initial = language_code
            self.fields['nickname'].widget.attrs['placeholder'] = _('Anonymous')
        else:
            self.fields['language'].initial = user_profile.language_code
            self.fields['interest_language'].initial = user_profile.interest_language_code
            self.fields['nickname'].initial = user_profile.nickname            
            self.fields['age'].initial = user_profile.age
            self.fields['gender'].initial = user_profile.gender
            self.fields['country'].initial = user_profile.country.code
            self.fields['terms_accepted'].initial = user_profile.terms_accepted

        form_labels_to_placeholders(self, exclude=('nickname',))

    def clean_age(self):        
        age = self.cleaned_data["age"]
        if age is not None and age < MIN_AGE:
            raise forms.ValidationError(_('You must be at least {min_age_in_years} years old to participate.'.format(min_age_in_years=MIN_AGE)))
        return age

    def save(self, commit=True):
        user_profile = self._user.get_profile()

        if self.cleaned_data['age']:
            user_profile.birth_date = subtract_years(datetime.date.today(), 
                                                     self.cleaned_data["age"])

        if self.cleaned_data['country']:
            user_profile.country = Country.objects.get(code=self.cleaned_data["country"])

        if self.cleaned_data["terms_accepted"]:
            terms = TermsOfService.objects.order_by('-effective_date')[0]
            user_profile.terms_accepted = terms.effective_date
        
        if self.cleaned_data["nickname"]:
            user_profile.nickname = self.cleaned_data["nickname"]

        if self.cleaned_data["language"]:
            user_profile.language_code = self.cleaned_data["language"]
            
        if self.cleaned_data["interest_language"]:
            user_profile.interest_language_code = self.cleaned_data["interest_language"]
            
        if commit:
            user_profile.save()

        return user_profile

class UserProfileCreationForm(UserProfileForm):
    def __init__(self, user, *args, **kwargs):
        super(UserProfileCreationForm, self).__init__(user, *args, **kwargs)
        for k, f in self.fields.iteritems():
            if k in ('terms_accepted', 'country', 'age'):
                f.required = True

class SignInForm(AuthenticationForm):
    def clean_username(self):
        return self.cleaned_data["username"].lower()

    def __init__(self, *args, **kwargs):
        super(SignInForm, self).__init__(*args, **kwargs)

        self.fields['username'].label = _('Email address')
        self.fields['password'].label = _('Password')

        form_labels_to_placeholders(self)        

class SignUpForm(UserCreationForm):
    language = forms.ChoiceField(label=_('Language'), choices=LANGUAGE_CODE_CHOICES, required=False)
    interest_language = forms.ChoiceField(label=_('Language'), choices=LANGUAGE_CODE_CHOICES, required=False)

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)

        language_code, country_code = [x.lower() for x in get_language().split('-')]
        self.fields['language'].initial = language_code
        self.fields['username'].label = _('Email address')
        self.fields['password'].label = _('New password')

        form_labels_to_placeholders(self)                

    def save(self, request=None):
        user = super(SignUpForm, self).save(request=request, commit=True)
        user_profile = user.get_profile()
        user_profile.language_code = self.cleaned_data["language"]        
        user_profile.interest_language_code = self.cleaned_data["interest_language"]
        user_profile.save()
        return user        

class PasswordResetForm(forms.Form):
    error_messages = {
        'unknown': _("That email address doesn't have an associated "
                     "user account. Are you sure you've registered?"),
        'unusable': _("The user account associated with this email "
                      "address cannot reset the password."),
    }
    email = forms.EmailField(label=_("Email address"), max_length=75)

    def clean_email(self):
        """
        Validates that an active user exists with the given email address.
        """
        email = self.cleaned_data["email"]
        self.users_cache = User.objects.filter(username__iexact=email,
                                               is_active=True)
        if not len(self.users_cache):
            raise forms.ValidationError(self.error_messages['unknown'])
        if any((user.password == UNUSABLE_PASSWORD)
               for user in self.users_cache):
            raise forms.ValidationError(self.error_messages['unusable'])
        return email

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        PasswordResetEmail(token_generator).create_mail(self.cleaned_data['email'])

class InviteForm(forms.Form):
    to_email = forms.EmailField(label=_("Email address"), max_length=75)

    def save(self, user):
        to_email = self.cleaned_data['to_email']
        if Invite.objects.filter(mail__to_email=to_email):
            raise ValueError('exists')
        invite = Invite()
        invite.user = user
        invite.create_mail(to_email)
        invite.save()
        return invite

RE_WS = re.compile('\s+')
RE_HASHTAG = re.compile('#(\w+)(?:\:(\w+))?')
    
def hashtag_value(v):
    return re.sub('\W', '', v).lower()

GENDERS = ('m', 'f', 'male', 'female')
COUNTRY_CODES = map(hashtag_value, Country.objects.values_list('code', flat=True))
COUNTRY_NAMES = map(hashtag_value, Country.objects.values_list('name', flat=True))

def clean_phrase_text(text):
    return RE_WS.sub(' ', text).strip()

def extract_hashtags(text):
    tags = {}
    for m in RE_HASHTAG.finditer(text):
        k, v = m.groups()
        # Guess key
        if v is None:
            v = k.lower()
            if v.isdigit():
                k = 'age'
            elif v in COUNTRY_CODES:
                k = 'country'
            elif v in COUNTRY_NAMES:
                k = 'country'
            elif v in GENDERS:
                k = 'gender'
            else:
                continue
        # Clean value
        v = v.lower()
        if k == 'age':
            v = int(v)
        elif k == 'country':
            if v in COUNTRY_CODES:
                pass
            elif v in COUNTRY_NAMES:
                v = COUNTRY_CODES[COUNTRY_NAMES.index(v)]
            else:
                v = ''  # invalid
        elif k == 'gender':
            if v in GENDERS:
                v = v[0]
            else:
                v = ''  # invalid
        tags[k] = v
    return clean_phrase_text(RE_HASHTAG.sub('', text)), tags

class PhraseRequestForm(forms.Form):
    text = forms.CharField(max_length=255)
    language_code = forms.CharField(max_length=2)

    def clean_text(self):
        text, self.cleaned_data["tags"] = extract_hashtags(self.cleaned_data["text"])
        return text
    
class PhraseResponseForm(forms.Form):
    text = forms.CharField(max_length=255, required=False)
    phrase_request_id = forms.IntegerField(required=False)
    favorited = forms.BooleanField(required=False)

    def clean_text(self):
        text, self.cleaned_data["tags"] = extract_hashtags(self.cleaned_data["text"])
        return text
