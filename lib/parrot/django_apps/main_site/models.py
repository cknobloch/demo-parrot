
import email
import binascii, os
import itertools
import datetime
import calendar

from collections import defaultdict
from smtplib import SMTPException

from django.db import models, transaction
from django.db.models.signals import post_save
from django.db.models import Q, Count, Sum
from django.contrib.auth.models import User
from django.utils import simplejson, timezone
from django.utils.translation import ugettext
from django.conf import settings
from django.core.mail import send_mail, EmailMultiAlternatives  #, EmailMessage, BadHeaderError
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.dispatch import receiver

from parrot.utils.language import *

import logging; logger = logging.getLogger('default')

NUM_AUDITS_TO_HIDDEN = 3

AUDIT_LIFETIME = 3

LANGUAGE_CODES_TO_NAMES = dict(LANGUAGE_CODES)

GENDERS = ('Male', 'Female')
GENDER_CHOICES = [(gender[0].lower(), gender) for gender in GENDERS]

SMTP_ERRORS = ('AuthenticationError', 'ConnectError', 'DataError', 'Exception', 'HeloError', 'RecipientsRefused', 'ResponseException', 'SenderRefused', 'ServerDisconnected')

NOTIFICATION_NAMES = ('',)

def make_choices(*choices):
    return zip(choices, choices)

class AuthToken(models.Model):
    user = models.OneToOneField(User, related_name='auth_token', null=True)
    key = models.CharField(max_length=40, primary_key=True)
    created = models.DateTimeField(auto_now_add=True)

    def generate_key(self):        
        return binascii.hexlify(os.urandom(20)).decode()

    def save(self, **kwargs):
        self.key = self.key or self.generate_key()
        return super(AuthToken, self).save(**kwargs)

@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        AuthToken.objects.create(user=instance)
        
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    nickname = models.CharField(null=True, max_length=30)
    email_verified = models.BooleanField(default=False)
    terms_accepted = models.DateField(null=True)
    gender = models.CharField(null=True, choices=GENDER_CHOICES, max_length=1)
    birth_date = models.DateField(null=True)
    country = models.ForeignKey('Country', null=True, related_name='+')
    language_code = models.CharField(null=True, max_length=2)
    interest_language_code = models.CharField(null=True, max_length=2)
    is_admin = models.BooleanField(default=False)
    ranking = models.IntegerField(null=True)

    _age = None
    @property
    def age(self):
        if self._age is None:
            self._age = self.birth_date and (datetime.date.today().year-self.birth_date.year)
        return self._age
    
    @property
    def language(self):
        return LANGUAGE_CODES_TO_NAMES[self.language_code]

    @property
    def interest_language(self):
        return LANGUAGE_CODES_TO_NAMES[self.interest_language_code]

    def tags(self):
        tags = {}
        if self.age is not None:
            tags['age'] = self.age
        if self.country is not None:
            tags['country'] = self.country.code
        if self.gender is not None:
            tags['gender'] = self.gender
        return tags

    def hashtags(self):
        return ' '.join(('#{}'.format(v)) for k, v in self.tags().iteritems())

    def signature(self):
        nickname = self.nickname or ugettext('Anonymous')
        return ' '.join(filter(None, (nickname, self.hashtags())))

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

class TermsOfService(models.Model):
    terms = models.TextField()
    effective_date = models.DateField()

class Mail(models.Model):
    to_email = models.EmailField()
    subject = models.CharField(null=True, max_length=78)
    body = models.TextField(null=True)
    smtp_error = models.CharField(null=True,
                                  max_length=max([len(e) for e in SMTP_ERRORS])+1,
                                  choices=zip(SMTP_ERRORS, SMTP_ERRORS))
    sent = models.DateTimeField(null=True)    
        
    def send(self, from_email=None):
        from_email = (from_email or
                      email.utils.formataddr((settings.NO_REPLY_DISPLAY_NAME,
                                              settings.NO_REPLY_EMAIL)))
        try:
            msg = EmailMultiAlternatives(self.subject, None, from_email, [self.to_email])
            msg.attach_alternative(self.body, "text/html")
            msg.send()
        except SMTPException, e:
            self.smtp_error = e.__class__.__name__[4:]
            logger.error(e)            
        else:
            self.smtp_error = None
        self.sent = timezone.now()
        self.save()

class Notification(models.Model):    
    name = models.CharField(max_length=max(len(x) for x in NOTIFICATION_NAMES)+1,
                            choices=zip(NOTIFICATION_NAMES, NOTIFICATION_NAMES))

class UserNotification(models.Model):
    user = models.ForeignKey(User, related_name='user_notifications')
    notification = models.ForeignKey(Notification, related_name='user_notifications')
    ack_date = models.DateTimeField(null=True)
    expiration = models.DateTimeField(null=True)
    created = models.DateTimeField(auto_now_add=True)

    def is_expired(self):
        return self.expiration <= timezone.now()

    def expire(self):
        self.ack_date = self.expiration
        self.save()

class Invite(models.Model):
    mail = models.ForeignKey(Mail, related_name='+')
    user = models.ForeignKey(User, related_name='invitations')

    def create_mail(self, to_email):
        from parrot.django_apps.main_site.emails import InviteEmail        
        self.mail = InviteEmail(user=self.user).create_mail(to_email)

    def save(self, *args, **kwargs):
        self.mail.save()
        super(Invite, self).save(*args, **kwargs)

class Country(models.Model):
    code = models.CharField(max_length=2)  # ISO-3166-1 alpha-2
    name = models.CharField(max_length=80)

    def __unicode__(self):
        return self.name
    
class Audit(models.Model):
    entity_id = models.IntegerField()
    entity_type = models.CharField(choices=make_choices('phrase_request', 'phrase_response'), max_length=30)
    user = models.ForeignKey(User, related_name='audits', null=True)
    created = models.DateTimeField(auto_now_add=True)

class Auditable:
    @classmethod
    def audits_for_class(cls, **filter_args):
        return Audit.objects.filter(entity_type=cls._meta.db_table, **filter_args)
    
    @property
    def audits(self):
        return Audit.objects.filter(entity_id=self.id, entity_type=self._meta.db_table)
    
    def mark_offensive(self, user):
        cls = self.__class__
        key_args = dict(entity_id=self.id, entity_type=self._meta.db_table)
        if not Audit.objects.filter(user=user, **key_args).exists():
            Audit.objects.create(user=user, **key_args)
            if len(self.audits.all()) >= NUM_AUDITS_TO_HIDDEN:
                self.hide()

    def hide(self):
        self.hidden = True

class Phrase(models.Model):
    text = models.CharField(max_length=255)    
    language_code = models.CharField(max_length=2)    

class PhraseRequest(models.Model, Auditable):
    user = models.ForeignKey(User, related_name='phrase_requests')
    phrase = models.ForeignKey(Phrase, related_name='phrase_requests')
    note = models.CharField(null=True, max_length=80)
    responder_gender = models.CharField(null=True, choices=GENDER_CHOICES, max_length=1)
    responder_age_min = models.IntegerField(null=True)
    responder_age_max = models.IntegerField(null=True)
    responder_country = models.ForeignKey('Country', null=True, related_name='phrase_requests')
    hidden = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)    
            
class PhraseResponse(models.Model, Auditable):
    phrase_request = models.ForeignKey(PhraseRequest, related_name='phrase_responses')
    user = models.ForeignKey(User, related_name='phrase_responses')
    phrase = models.ForeignKey(Phrase, related_name='phrase_responses')
    hidden = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
        
    _clip_storage = None
    @property
    def clip_storage(self):
        if self._clip_storage is None:
            storage_settings = settings.PHRASE_RESPONSE_FILE_STORAGE
            module_path, class_name = storage_settings['class'].rsplit('.', 1)
            module = __import__(module_path, fromlist=[class_name]) 
            storage = getattr(module, class_name)(**storage_settings.get('settings', {}))
            self._clip_storage = storage
        return self._clip_storage

    @property
    def clip_name(self):
        return self.clip_storage.get_valid_name('{}.wav'.format(self.id))
        
    @property
    def clip_url(self):
        return self.clip_storage.url(self.clip_name)

    @property
    def clip_upload_url(self):
        def url(self, name):
            name = self._normalize_name(self._clean_name(name))
            if self.custom_domain:
                return "%s//%s/%s" % (self.url_protocol,
                                      self.custom_domain, name)
            return self.connection.generate_url(self.querystring_expire,
                method='PUT', bucket=self.bucket.name, key=self._encode_name(name),
                query_auth=self.querystring_auth, force_http=not self.secure_urls)
        return url(self.clip_storage, self.clip_name)

    def save_clip_to_storage(self, content):
        return self.clip_storage.save(self.clip_name, ContentFile(content))    

class PhraseResponseFavorite(models.Model):
    user = models.ForeignKey(User, related_name='phrase_response_favorites')
    phrase_response = models.ForeignKey(PhraseResponse, related_name='phrase_response_favorites')
