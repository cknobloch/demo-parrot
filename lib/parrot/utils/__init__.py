
import os
import sys

class SingleInstanceAssertionError(Exception):
    pass

class SingleInstanceAssertion(object):
    def __init__(self, sourcefile=None, pidfile=None):
        self.pidfile = pidfile or '/tmp/{}.pid'.format(os.path.splitext(os.path.basename(sourcefile))[0])
        
    def __enter__(self):
        if os.path.isfile(self.pidfile):
            raise SingleInstanceAssertionError(self.pidfile)
        file(self.pidfile, 'w').write(str(os.getpid()))
        return self

    def __exit__(self, type, value, tb):
        os.unlink(self.pidfile)
