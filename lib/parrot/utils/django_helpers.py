
import datetime
import time
import calendar

from django.http import HttpResponse
from django.utils.decorators import available_attrs
from django.utils import simplejson, timezone
from django import forms
from django.db.models.fields.related import ManyToManyField
from django.db import models, connection, transaction
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session

from functools import wraps

DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
DATE_FORMAT     = '%Y-%m-%d'

def refresh_from_db(model, query_set=None):
    """Refreshes this instance from db
    https://code.djangoproject.com/ticket/901
    """
    query_set = query_set or model.__class__.objects
    from_db = query_set.get(pk=model.pk)
    for field in model.__class__._meta.fields:
        setattr(model, field.name, getattr(from_db, field.name))

def format_datetime_html(dt):
    return format_datetime(dt, 
                           dt_fmt='<span class="datetime">{date_fmt}{time_fmt}</span>',
                           date_fmt='<span class="date">%x</span>',
                           time_fmt='<span class="time">%X</span>')

def format_datetime(dt, dt_fmt=None, date_fmt='%x', time_fmt='%X'):
    dt_fmt = (dt_fmt or '{date_fmt} {time_fmt}').format(date_fmt=date_fmt, time_fmt=time_fmt)

    if isinstance(dt, datetime.datetime):
        now = timezone.now()
        if (dt.day   == now.day   and 
            dt.month == now.month and 
            dt.year  == now.year):
            fmt = time_fmt
        else:
            fmt = dt_fmt
    else:
        fmt = date_fmt

    return timezone.localtime(dt).strftime(fmt)

class JsonResponse(HttpResponse):
    def __init__(self, content, mimetype='application/json', status=None, content_type=None):
        super(JsonResponse, self).__init__(
            content=simplejson.dumps(content),
            mimetype=mimetype,
            status=status,
            content_type=content_type,
        )

def to_simple_repr(x, format=None):    
    """Returns the value in a simple representation (JSON-friendly)."""
    format = format or to_simple_repr
    if x is None:
        return x
    elif isinstance(x, (basestring, int, long, float, bool)):
        return x
    elif isinstance(x, (list, tuple)):
        return [format(v) for v in x]
    elif isinstance(x, dict):
        return {k: format(v) for k, v in x.iteritems()}
    elif isinstance(x, datetime.datetime):
        return x.strftime(DATETIME_FORMAT)
    elif isinstance(x, datetime.date):
        return x.strftime(DATE_FORMAT)
    else:
        return str(x)

def to_js_repr(x):
    if   isinstance(x, (datetime.datetime, datetime.date, datetime.time)):        
        return calendar.timegm(x.timetuple()) * 1000  # time.mktime
    else:
        return to_simple_repr(x, format=to_js_repr)

def from_simple_repr(x):    
    """ """
    if x is None:
        return x
    elif isinstance(x, (int, float, bool)):
        return x
    elif isinstance(x, (list, tuple)):
        return [from_simple_repr(v) for v in x]
    elif isinstance(x, dict):
        return {k: from_simple_repr(v) for k, v in x.iteritems()}
    elif isinstance(x, basestring):
        try:
            return timezone.make_aware(datetime.datetime.strptime(x, DATETIME_FORMAT), 
                                       timezone.utc)
        except ValueError:
            try:
                return datetime.datetime.strptime(x, DATE_FORMAT).date()
            except ValueError:
                return x
    else:
        raise ValueError('Unxpected type: {}'.format(x.__class__))

def simple_resource_to_response(resource):
    if resource:
        return JsonResponse(to_simple_repr(resource))
    else:
        return HttpResponse(status=404)

def js_resource_to_response(resource):
    if resource:        
        return JsonResponse(to_js_repr(resource))
    else:
        return HttpResponse(status=404)

def authenticated_or_401(function=None):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):            
            if request.user.is_authenticated():
                return view_func(request, *args, **kwargs)
            return HttpResponse(status=401)
        return _wrapped_view

    if function:
        return decorator(function)
    else:
        return decorator

# COPIED FROM django.forms.models, commented "f.editable" check
def model_to_dict(instance, fields=None, exclude=None):
    """
    Returns a dict containing the data in ``instance`` suitable for passing as
    a Model's keyword arguments.

    ``fields`` is an optional list of field names. If provided, only the named
    fields will be included in the returned dict.

    ``exclude`` is an optional list of field names. If provided, the named
    fields will be excluded from the returned dict, even if they are listed in
    the ``fields`` argument.
    """
    opts = instance._meta
    data = {}
    for f in opts.fields + opts.many_to_many:
        # if not f.editable:
        #     continue
        if fields and not f.name in fields:
            continue
        if exclude and f.name in exclude:
            continue
        if isinstance(f, ManyToManyField):
            # If the object doesn't have a primary key yet, just use an empty
            # list for its m2m fields. Calling f.value_from_object will raise
            # an exception.
            if instance.pk is None:
                data[f.name] = []
            else:
                # MultipleChoiceWidget needs a list of pks, not object instances.
                data[f.name] = [obj.pk for obj in f.value_from_object(instance)]
        else:
            data[f.name] = f.value_from_object(instance)
    return data

# http://djangosnippets.org/snippets/833/
class LockingManager(models.Manager):
    """ Add lock/unlock functionality to manager.    
    Example::    
        class Job(models.Model):        
            manager = LockingManager()    
            counter = models.IntegerField(null=True, default=0)    
            @staticmethod
            def do_atomic_update(job_id)
                ''' Updates job integer, keeping it below 5 '''
                try:
                    # Ensure only one HTTP request can do this update at once.
                    Job.objects.lock()                    
                    job = Job.object.get(id=job_id)
                    # If we don't lock the tables two simultanous
                    # requests might both increase the counter
                    # going over 5
                    if job.counter < 5:
                        job.counter += 1                                        
                        job.save()                
                finally:
                    Job.objects.unlock()        
    """    

    def lock(self):
        """ Lock table. 
        
        Locks the object model table so that atomic update is possible.
        Simulatenous database access request pend until the lock is unlock()'ed.
        
        Note: If you need to lock multiple tables, you need to do lock them
        all in one SQL clause and this function is not enough. To avoid
        dead lock, all tables must be locked in the same order.
        
        See http://dev.mysql.com/doc/refman/5.0/en/lock-tables.html
        """
        cursor = connection.cursor()
        cursor.execute("LOCK TABLES %s WRITE" % self.model._meta.db_table)
        return cursor.fetchone()
        
    def unlock(self):
        """ Unlock the table. """
        cursor = connection.cursor()
        cursor.execute("UNLOCK TABLES")
        return cursor.fetchone()

    def __enter__(self):
        self.lock()
        
    def __exit__(self, type, value, traceback):
        self.unlock()

@transaction.commit_manually
def flush_transaction():
    """
    Flush the current transaction so we don't read stale data

    Remove if upgraded to Django 1.6!!

    Use in long running processes to make sure fresh data is read from
    the database.  This is a problem with MySQL and the default
    transaction mode.  You can fix it by setting
    "transaction-isolation = READ-COMMITTED" in my.cnf or by calling
    this function at the appropriate moment.

    'OPTIONS': { "init_command": "SET storage_engine=INNODB, SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED", }

    http://stackoverflow.com/questions/3346124/how-do-i-force-django-to-ignore-any-caches-and-reload-data
    """
    transaction.commit()

# def get_all_logged_in_users():
#     user_ids = []
#     sessions = Session.objects.filter(expire_date__gte=datetime.datetime.now())    
#     for session in sessions:
#         user_ids.append(session.get_decoded().get('_auth_user_id', None))
#     return User.objects.filter(id__in=user_ids)

from functools import wraps
import time

def memoize(timeout=0, _cache=None, num_args=None):    
    def decorator(f):
        @wraps(f)
        def _memoize(*args, **kw):
            if _cache is None and not hasattr(f, '_cache'):
                f._cache = {}
            cache = _cache or f._cache
            mem_args = args[:num_args]
            if kw: 
                key = mem_args, frozenset(kw.iteritems())
            else:
                key = mem_args
            if key in cache:
                result, timestamp = cache[key]
                age = time.time() - timestamp
                if not timeout or age < timeout:
                    return result
            result = f(*args, **kw)
            cache[key] = (result, time.time())
            return result
        return _memoize
    return decorator

def format_metric(value, rounding=False):
    """
    Returns metric formatted using common abbreviations to shorten the value.  
    In English: k, m, b and t as abbreviations for 
    thousand, million, billion and trillion.
    """ 
    return value  # TODO -- define this function!

class LastRequestMiddleware(object):
    def process_request(self, request):
        if hasattr(request, 'session'):
            request.session['last_request_time'] = datetime.datetime.now()
