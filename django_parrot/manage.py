#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    # Add "lib" folder to import list
    pwd = os.path.dirname(os.path.realpath(__file__))
    sys.path.append(os.path.join(pwd, '..', 'lib'))

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "parrot_project.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
