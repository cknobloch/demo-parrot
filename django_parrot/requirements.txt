Django==1.4.19
MySQL-python==1.2.3
mock==1.0.1
pytz==2013.9
wsgiref==0.1.2
django-less==0.7.2
django-pipeline==1.3.16
