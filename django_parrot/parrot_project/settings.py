# Django settings for parrot project.

import os

# Added for django-less support
LESS_ROOT = '../lib/parrot/django_apps/main_site/static/'
LESS_OUTPUT_DIR = 'cache'

PROTOCOL, DOMAIN = 'http', os.environ.get('DOMAIN')

DEBUG  = os.environ.get('DEBUG', True)
PUBLIC = os.environ.get('PUBLIC', True)

ADMINS = (
    ('CrowdPhrase', 'support@crowdphrase.com'),
)

NO_REPLY_EMAIL = 'noreply@crowdphrase.com'
NO_REPLY_DISPLAY_NAME = 'CrowdPhrase'
DEFAULT_FROM_EMAIL = NO_REPLY_EMAIL

MANAGERS = ADMINS

# SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

DATABASES = {
    'default': {
        'ENGINE'  : 'django.db.backends.mysql',
        'NAME'    : os.environ['RDS_DB_NAME'] or 'parrot',
        'USER'    : os.environ['RDS_USERNAME'],
        'PASSWORD': os.environ['RDS_PASSWORD'],
        'HOST'    : os.environ['RDS_HOSTNAME'],
        'PORT'    : os.environ['RDS_PORT'],
        'OPTIONS' : {"init_command": "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED",},
        'TEST_CHARSET': 'UTF8'
    }
}

ROOT_PATH = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../..'))

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/New_York'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Authentication URLs
# Example: "/accounts/login/"
LOGIN_REDIRECT_URL = '/member/'
LOGIN_URL  = '/login/'
LOGOUT_URL = '/logout/'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = '../media/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '{}/static/'.format(ROOT_PATH)

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/s/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'p)0zp%!no$8+4vei8$$!&amp;+1)n4m(zkhtp@dhzb)avw2*s@qg62'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'parrot.django_apps.main_site.context_processors.base_context',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
)

MIDDLEWARE_CLASSES = (
    'pipeline.middleware.MinifyHTMLMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'parrot.utils.django_helpers.LastRequestMiddleware',        
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.gzip.GZipMiddleware',            
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'parrot.django_apps.main_site.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'parrot_project.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'parrot.django_apps.main_site',
    'pipeline',
    'less',
    'storages',
    # Django apps
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler'
        },
        'error_file': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': os.path.join(ROOT_PATH, 'logs/error.log')
        },
        'debug_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(ROOT_PATH, 'logs/debug.log')
        },        
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['console', 'mail_admins'],
            'level': 'ERROR',
            'propagate': True
        },
        'default': {
            'handlers': ['console'],
            'level': 'INFO',
        }
    }
}

AUTH_PROFILE_MODULE = 'main_site.UserProfile'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

if DEBUG:
    EMAIL_HOST = 'email-smtp.us-east-1.amazonaws.com'
    EMAIL_PORT = 587
    EMAIL_HOST_USER = 'XXX'
    EMAIL_HOST_PASSWORD = 'XXX'
    EMAIL_USE_TLS = True
else:
    EMAIL_HOST = 'smtp.sendgrid.net'
    EMAIL_PORT = 587
    EMAIL_HOST_USER = 'XXX'
    EMAIL_HOST_PASSWORD = 'XXX'
    EMAIL_USE_TLS = True

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/django_cache',
    },
    'test': {  # used by unit tests
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'test',
    }
}

# if DEBUG or vars().get('MINIMAL_SETUP'):
#     CACHES['message_cache'] = CACHES['default']
# else:
#     try:
#         import pylibmc
#         memcached_module = 'PyLibMCCache'
#     except ImportError:
#         memcached_module = 'MemcachedCache'

#     CACHES['message_cache'] = {
#         'BACKEND': 'django.core.cache.backends.memcached.{}'.format(memcached_module),
#         'LOCATION': os.environ['MESSAGE_CACHE_LOCATION'],
#     }

STATICFILES_STORAGE = 'pipeline.storage.PipelineStorage'
PIPELINE_DISABLE_WRAPPER = True  # allow pollution, for library JS files
PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.uglifyjs.UglifyJSCompressor'
PIPELINE_UGLIFYJS_BINARY = '{}/lib/UglifyJS/bin/uglifyjs'.format(ROOT_PATH)
PIPELINE_UGLIFYJS_ARGUMENTS = '--no-mangle-functions'
PIPELINE_JS = {
    'member_home': {
        'source_filenames': (
            'js/recorder.js',
            'js/phrase.js',
        ),
        'output_filename': 'js/member_home-min.js',
    },
    'home': {
        'source_filenames': (
            'js/util.js',
        ),
        'output_filename': 'js/home-min.js',
    }
}

if False:  # DEBUG or vars().get('MINIMAL_SETUP'):
    PHRASE_RESPONSE_FILE_STORAGE = {
        'class': 'django.core.files.storage.FileSystemStorage'
    }
else:
    import boto.s3.connection
    PHRASE_RESPONSE_FILE_STORAGE = {
        'class': 'storages.backends.s3boto.S3BotoStorage',
        'settings': {
            'access_key' : 'AKIAJI2VZUOJKWBYMD6Q',
            'secret_key' : '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',
            'bucket_name': 'parrot.chat-operated-us-west-1',
            'secure_urls': False,
            'querystring_expire': 60*5,        
            #'calling_format': boto.s3.connection.OrdinaryCallingFormat(),
            # s3.CallingFormat.SUBDOMAIN
        }
    }
    if not DEBUG:
        PHRASE_RESPONSE_FILE_STORAGE['settings']['bucket_name'] = 'parrot-us-west-1'
